#pragma once

#include <functional>

using namespace std;

// execution timing, seconds
double timeTask(function<void(void)> task);
