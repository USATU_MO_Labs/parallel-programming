#include "stdafx.h"

#include <vector>

#include "utils.h"
#include "task4.h"


using namespace std;


// quite useless and synthetic example of barrier synchronization
void barrier_job()
{
    int resultTuple[4] = { 0, 0, 0, 0 };

    #pragma omp parallel num_threads(4)

    #pragma omp sections
    {
        #pragma omp section
        {
            doSomethingPrimitive();
            doSomethingPrimitive();
            doSomethingPrimitive();
            doSomethingPrimitive();
            resultTuple[0] = 1;
            cout << "Section 1 done\n";
        }

        #pragma omp section
        {
            doSomethingPrimitive();
            doSomethingPrimitive();
            doSomethingPrimitive();
            resultTuple[1] = 2;
            cout << "Section 2 done\n";
        }

        #pragma omp section
        {
            doSomethingPrimitive();
            doSomethingPrimitive();
            resultTuple[2] = 3;
            cout << "Section 3 done\n";
        }

        #pragma omp section
        {
            doSomethingPrimitive();
            resultTuple[3] = 4;
            cout << "Section 4 done\n";
        }
    }

    #pragma omp barrier

    #pragma omp for
    for (int i = 0; i < 4; i++) {
        resultTuple[i] = 0;
    }
}
