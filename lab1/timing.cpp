#include "stdafx.h"
#include "timing.h"
#include <omp.h>

using namespace std;


// execution timing, seconds
double timeTask(function<void(void)> task) {
    auto start = omp_get_wtime();

    // TODO multiple executions and benchmarking?
    task();

    auto end = omp_get_wtime();

    return end - start;
}
