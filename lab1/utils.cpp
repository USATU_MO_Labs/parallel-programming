#include "stdafx.h"
#include "utils.h"

// primitive task
void doSomethingPrimitive() {
    int someVar = 0;
    for (auto i = 0; i < 100; i++) someVar += i;
}
