#include "task3.h"
#include "stdafx.h"

int sum_critical(int *arr, int length)
{
    int result = 0;

    #pragma omp parallel
    {
        int local_result = 0;

        #pragma omp for
        for (int i = 0; i < length; i++) {
            local_result += arr[i];
        }

        #pragma omp critical
        result += local_result;
    }

    return result;
}

int sum_reduction(int *arr, int length)
{
    int result = 0;

    #pragma omp parallel for reduction(+: result)
    for (int i = 0; i < length; i++) {
        result += arr[i];
    }

    return result;
}
