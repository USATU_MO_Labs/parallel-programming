// lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "timing.h"

#include "task2.h"
#include "task3.h"
#include "task4.h"
#include "task5.h"


using namespace std;


// output `hello world` in parallel 5-10 times
void task1()
{
    #pragma omp parallel num_threads(10)
    cout << "Hello world!\n";
}


// explore schedule options with different loops
void task2()
{
    double timings[3][4] = {
        {
            timeTask(&loop_uniform_static),
            timeTask(&loop_uniform_dynamic),
            timeTask(&loop_uniform_guided),
            timeTask(&loop_uniform_runtime)
        },
        {
            timeTask(&loop_odd_static),
            timeTask(&loop_odd_dynamic),
            timeTask(&loop_odd_guided),
            timeTask(&loop_odd_runtime)
        },
        {
            timeTask(&loop_half_static),
            timeTask(&loop_half_dynamic),
            timeTask(&loop_half_guided),
            timeTask(&loop_half_runtime)
        },
    };

    cout << "(in microseconds):" << endl;
    
    // column names
    printf("%10s ", "loop type");
    printf("%10s ", "static");
    printf("%10s ", "dynamic");
    printf("%10s ", "guided");
    printf("%10s ", "runtime");
    printf("\n");

    for (int i = 0; i < 3; i++) {

        // row name, quick and dirty
        switch (i) {
        case 0:
            printf("%10s ", "uniform");
            break;
        
        case 1:
            printf("%10s ", "odd");
            break;

        case 2:
            printf("%10s ", "half");
            break;
        }

        // row
        for (int j = 0; j < 4; j++) {
            auto time = timings[i][j] * 100000;
            printf("%10.3f ", time);
        }

        printf("\n");
    }
}


// array sum with critical sections and reduction
void task3()
{
    const int N = 1000;
    int* arr = new int[1000];

    for (int i = 0; i < N; i++) {
        arr[i] = rand() % 100;
    }

    cout << "Parallel sum:" << endl;

    auto time_critical = timeTask([arr, N]() { sum_critical(arr, N); });
    cout << "\twith critical sections: " << time_critical * 100000 << " microseconds" << endl;

    auto time_reduction = timeTask([arr, N]() { sum_reduction(arr, N); });
    cout << "\twith reductions: " << time_reduction * 100000 << " microseconds" << endl;

    delete[] arr;
}


void task4()
{
    barrier_job();
}


void task5()
{
    // create array
    const int N = 100;
    int** arr = new int*[N];
    for (int i = 0; i < N; i++) {
        arr[i] = new int[N];
    }

    // array filler lambda
    auto fillArray = [N, arr]()
    {
        #pragma omp parallel for
        for (int i = 0; i < 100; i++)
        {
            #pragma omp parallel for
            for (int j = 0; j < 100; j++)
            {
                arr[i][j] = (i + j) % 10;
            }
        }
    };

    fillArray();
    auto unparallelized_time = timeTask([arr, N]() { unparallelized_loop(arr, N); });
    
    cout << "Unparallelized loop: " << unparallelized_time * 100000 << " microseconds" << endl;

    fillArray();
    auto parallelized_time = timeTask([arr, N]() { parallelized_loop(arr, N); });

    cout << "Parallelized loop: " << parallelized_time * 100000 << " microseconds" << endl;

    // delete array
    for (int i = 0; i < N; i++) {
        delete[] arr[i];
    }
    delete[] arr;
}


int main()
{
    cout << "Task 1. Output `Hello world!` in parallel 5-10 times" << endl;
    task1();
    cout << endl;

    cout << "Task 2. Explore schedule options with different loops" << endl;
    task2();
    cout << endl;

    cout << "Task 3. Array sum in parallel with critical sections and reductions" << endl;
    task3();
    cout << endl;

    cout << "Task 4. Barrier example" << endl;
    task4();
    cout << endl;

    cout << "Task 5. Parallelize loop" << endl;
    task5();
    cout << endl;

    cin.get();

    return 0;
}

