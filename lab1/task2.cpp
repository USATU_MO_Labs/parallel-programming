#include "stdafx.h"
#include "task2.h"
#include "utils.h"


using namespace std;


// number of iterations for each loop
const int N = 1000;


// UNIFORM TIMING


// static schedule, uniform timing
void loop_uniform_static()
{
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < N; i++) {
        doSomethingPrimitive();
    }
}

// dynamic schedule, uniform timing
void loop_uniform_dynamic()
{
    #pragma omp parallel for schedule(dynamic)
    for (int i = 0; i < N; i++) {
        doSomethingPrimitive();
    }
}

// guided schedule, uniform timing
void loop_uniform_guided()
{
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < N; i++) {
        doSomethingPrimitive();
    }
}

// runtime schedule, uniform timing
void loop_uniform_runtime()
{
    #pragma omp parallel for schedule(runtime)
    for (int i = 0; i < N; i++) {
        doSomethingPrimitive();
    }
}


// NON-UNIFORM TIMING - ODD ITERATIONS TAKE LONGER


// static schedule, non-uniform timing 1 - odd
void loop_odd_static()
{
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < N; i++) {
        if (i % 2 == 0) {
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
    }
}

// dynamic schedule, non-uniform timing 1 - odd
void loop_odd_dynamic()
{
    #pragma omp parallel for schedule(dynamic)
    for (int i = 0; i < N; i++) {
        if (i % 2 == 0) {
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
    }
}

// guided schedule, non-uniform timing 1 - odd
void loop_odd_guided()
{
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < N; i++) {
        if (i % 2 == 0) {
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
    }
}

// runtime schedule, non-uniform timing 1 - odd
void loop_odd_runtime()
{
    #pragma omp parallel for schedule(runtime)
    for (int i = 0; i < N; i++) {
        if (i % 2 == 0) {
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
    }
}


// NON-UNIFORM TIMING - FIRST HALF OF ITERATIONS TAKES LONGER THAN THE LAST


// static schedule, non-uniform timing 2 - half
void loop_half_static()
{
    int N_half = N / 2;
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < N; i++) {
        if (i < N_half) {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
        }
    }
}

// dynamic schedule, non-uniform timing 2 - half
void loop_half_dynamic()
{
    int N_half = N / 2;
    #pragma omp parallel for schedule(dynamic)
    for (int i = 0; i < N; i++) {
        if (i < N_half) {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
        }
    }
}

// guided schedule, non-uniform timing 2 - half
void loop_half_guided()
{
    int N_half = N / 2;
    #pragma omp parallel for schedule(guided)
    for (int i = 0; i < N; i++) {
        if (i < N_half) {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
        }
    }
}

// runtime schedule, non-uniform timing 2 - half
void loop_half_runtime()
{
    int N_half = N / 2;
    #pragma omp parallel for schedule(runtime)
    for (int i = 0; i < N; i++) {
        if (i < N_half) {
            doSomethingPrimitive();
            doSomethingPrimitive();
        }
        else {
            doSomethingPrimitive();
        }
    }
}