#pragma once

void loop_uniform_static();
void loop_uniform_dynamic();
void loop_uniform_guided();
void loop_uniform_runtime();

void loop_odd_static();
void loop_odd_dynamic();
void loop_odd_guided();
void loop_odd_runtime();

void loop_half_static();
void loop_half_dynamic();
void loop_half_guided();
void loop_half_runtime();
