#include "stdafx.h"
#include "task5.h"
#include <omp.h>

void unparallelized_loop(int** arr, int length)
{
    for (int i = 2; i < length; i++) 
    {
        for (int j = 2; j < length; j++) 
        {
            arr[i][j] = arr[i - 2][j] + arr[i][j - 2];
        }
    }
}

void parallelized_loop(int** arr, int length)
{
    auto prevOmpNested = omp_get_nested();

    omp_set_nested(1);

    #pragma omp parallel for num_threads(2) schedule(static)
    for (int i = 2; i < length; i++) 
    {
        #pragma omp parallel for num_threads(2) schedule(static)
        for (int j = 2; j < length; j++) 
        {
            arr[i][j] = arr[i - 2][j] + arr[i][j - 2];
        }
    }

    omp_set_nested(prevOmpNested);
}
